FROM centos

RUN yum install -y gcc openssl-devel make

RUN curl -O http://download.joedog.org/siege/siege-latest.tar.gz && \
    tar xvzf siege-latest.tar.gz && \
    rm -f siege-latest.tar.gz && \
    cd siege-* && \
    ./configure && \
    make && \
    make install
